import sbt._

object Dependencies {
  lazy val scriptedPlugin = "org.scala-sbt" %% "scripted-plugin" % "1.3.0"
  def pluginDeps = Seq(scriptedPlugin)
}

