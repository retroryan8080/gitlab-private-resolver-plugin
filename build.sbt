import Dependencies._
import sbt._
import Keys._

lazy val scala212 = "2.12.10"
lazy val scala213 = "2.13.0"
lazy val supportedScalaVersions = List(scala212, scala213)

ThisBuild / scalaVersion     := scala212
ThisBuild / version          := "1.0.0"
ThisBuild / organization     := "com.nimbleus.glresolver"
ThisBuild / organizationName := "nimbleus"

ThisBuild / licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))

lazy val root = (project in file("."))
  .settings(
    name := "gitlab-private-resolver-plugin",
    sbtPlugin := true,
    crossScalaVersions := supportedScalaVersions,
    libraryDependencies ++= pluginDeps,
    publishMavenStyle := false,
    bintrayRepository := "gitlab-private-resolver-plugin",
    bintrayOrganization in bintray := None
  )
